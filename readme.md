# Linux Kernel for MOSTRO

This is a set of scripts that allow for building a Linux kernel for the Beaglebone. Forked from https://github.com/robertcnelson/ti-linux-kernel-dev with slight modifications for the MOSTRO. It applies real-time and Texas Instruments patches to the mainline Linux kernel.

To compile the first time, do the following. It'll download a lot of stuff and will take long.

    git checkout mostro
    ./build_kernel.sh

Probably useful:

    tools/rebuild.sh
    tools/rebuild_modules.sh

When the config utility comes up, just choose exit (use default config). The kernel, firmware and modules will be created at `deploy/`. 

For info on how to hack on an existing module, see https://gitlab.com/outer-space-sounds/mostro/tlv320aic3x-codec-driver.

To use the created kernel and modules, you need a bootloader and a Linux file system. That stuff is at https://gitlab.com/outer-space-sounds/mostro/linux-filesystem

# Original README from Robert Nelson:

This is just a set of scripts to rebuild a known working kernel for ARM devices.

Script Bugs: "bugs@rcn-ee.com"

Note, for older git tag's please use: https://github.com/RobertCNelson/yakbuild

Dependencies: GCC ARM Cross ToolChain

Linaro:
http://www.linaro.org/downloads/

Dependencies: Linux Kernel Source

This git repo contains just scripts/patches to build a specific kernel for some
ARM devices. The kernel source will be downloaded when you run any of the build
scripts.

By default this script will clone the linux-stable tree:
https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
to: ${DIR}/ignore/linux-src:

If you've already cloned torvalds tree and would like to save some hard drive
space, just modify the LINUX_GIT variable in system.sh to point to your current
git clone directory.

Build Kernel Image:

```
./build_kernel.sh
```

Optional: Build Debian Package:

```
./build_deb.sh
```

Development/Hacking:

first run (to setup baseline tree): ./build_kernel.sh
then modify files under KERNEL directory
then run (to rebuild with your changes): ./tools/rebuild.sh

